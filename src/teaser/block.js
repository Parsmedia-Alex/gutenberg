import "./editor.scss";

const { apiFetch } = wp;

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType} = wp.blocks;
const { InnerBlocks, InspectorControls } = wp.editor;
const { withSelect, select } = wp.data;
const { Component, Fragment } = wp.element;
const { withState } = wp.compose;

const { PanelBody, CheckboxControl, SelectControl, TextControl, TextareaControl, Modal, Button  } = wp.components;


String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}


class teaserBlockComponent extends Component
{
  constructor() {
    super(...arguments);
    this.state = this.constructor.getInitialState(this.props.attributes);

    this.updatePostTypes = this.updatePostTypes.bind(this);
    this.onChangePostType = this.onChangePostType.bind(this);

    this.onAddPost = this.onAddPost.bind(this);
    this.onChangeTeaser = this.onChangeTeaser.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeExcerpt = this.onChangeExcerpt.bind(this);
    this.onChangeButton = this.onChangeButton.bind(this);

    this.deleteTeaser = this.deleteTeaser.bind(this);

    this.getPosts = this.getPosts.bind(this);

    apiFetch.use(apiFetch.createNonceMiddleware(wpApiSettings.nonce));
    apiFetch.use(apiFetch.createRootURLMiddleware(wpApiSettings.root));
  }

  static getInitialState(attributes)
  {
    let teaser = attributes.teaser ? JSON.parse(attributes.teaser) : [];
    let webp = attributes.enable_webp ? true : false;

    console.log(attributes.post_type);

    return {
      id: teaser ? teaser.length : 0,
      enable_webp: webp,
      disable_select: false,
      teaser: teaser,
      posts: [],
      post_type: attributes.post_type,
      post_types : [
        { value: 'posts', label: __("Post") },
        { value: 'pages', label: __("Page") }
      ]
    };
  }

  componentDidMount()
  {
    if(this.state.teaser.length > 0)
      this.setState({disable_select: true});

    this.updatePostTypes();
    this.getPosts();
  }

  getPosts()
  {
    console.log("getPosts " + this.props.attributes.post_type);

    apiFetch({ path: `/wp/v2/${this.props.attributes.post_type}?_embed` } ).then( posts => {
      if(posts)
      this.setState({ posts });
    });

  }

  updatePostTypes()
  {
    console.log("updatePostTypes");

    //ignores attachment, wp_block
    let ignore_post_types = ["attachment", "wp_block"];
    let custom_post_types = [];

    apiFetch( { path: "/wp/v2/types" } ).then( types => {

      for(var type in types)
      {
        if(!ignore_post_types.includes(type))
        {
          custom_post_types.push({value: types[type].rest_base, label: types[type].name});
        }
      }

      this.setState({ post_types: custom_post_types });
    });
  }

  onChangePostType(value) {

    console.log("onChangePostType " + value);
    this.props.setAttributes( { post_type: value } );

    setTimeout(() => {
        this.getPosts();
    }, 200);

  }

  onAddPost() {
    console.log("onAddPost");

    if(this.state.posts.length <= 0)
      this.getPosts();

    this.setState(prevState => ({
      teaser: [...prevState.teaser, { id:  this.state.id, selected_post: 0, url: "", image: {id: 0, src: ""}, title: "", excerpt: "", button_text: this.props.attributes.default_button_text}]
    }));

    setTimeout(() => {
      this.state.id++;
    }, 200);
  }

  onChangeTeaser(e, key) {
    console.log("onChangeTeaser " + e + " " + key);

    const post = this.state.posts.find((item) => { return item.id == parseInt(e) } );

    //console.log(post._embedded["wp:featuredmedia"][0]["media_details"]["sizes"]["teaser-size"].source_url);

    let new_teaser = this.state.teaser.slice();

    new_teaser[key].url = post.link;
    new_teaser[key].image = {id: post.featured_media,  src: post._embedded["wp:featuredmedia"][0]["media_details"]["sizes"]["teaser-size"].source_url };
    new_teaser[key].title = post.title.rendered;
    new_teaser[key].excerpt = $(post.excerpt.rendered).text();
    new_teaser[key].selected_post = post.id;

    this.setState({teaser: new_teaser});

    let teaser_json = JSON.stringify(new_teaser);

    this.props.setAttributes( { teaser:  teaser_json} );
  }

  onChangeTitle(value, key) {
    console.log("onChangeTitle " + value + " " + key);

    let new_teaser = this.state.teaser.slice();

    new_teaser[key].title = value;

    this.setState({teaser: new_teaser});

    let teaser_json = JSON.stringify(new_teaser);

    this.props.setAttributes( { teaser:  teaser_json} );
  }

  onChangeExcerpt(value, key) {
    console.log("onChangeExcerpt " + value + " " + key);

    let new_teaser = this.state.teaser.slice();

    new_teaser[key].excerpt = value;

    this.setState({teaser: new_teaser});

    let teaser_json = JSON.stringify(new_teaser);

    this.props.setAttributes( { teaser:  teaser_json} );
  }

  onChangeButton(value, key) {
    console.log("onChangeButton " + value + " " + key);

    let new_teaser = this.state.teaser.slice();

    new_teaser[key].button_text = value;

    this.setState({teaser: new_teaser});

    let teaser_json = JSON.stringify(new_teaser);

    this.props.setAttributes( { teaser:  teaser_json} );
  }

  deleteTeaser(e, key) {
    console.log("deleteTeaser " + e + " " + key);

    let new_teaser = this.state.teaser.slice();

    new_teaser.splice(key, 1);

    this.setState({teaser: new_teaser});

    let teaser_json = JSON.stringify(new_teaser);

    this.props.setAttributes( { teaser: teaser_json } );
  }


  render()
  {
    let options = [ { value: 0, label: __( "Select one" ) } ];

    if(this.state.teaser.length)
    {
      this.state.posts.forEach((post) => {
        options.push( { value:post.id, label:post.title.rendered } );
      });
    }

    return (

      <Fragment>
      <InspectorControls>
      <PanelBody opened = { true } >
      <Button onClick = { this.onAddPost } isDefault style = { {"display": "block", "marginLeft": "auto" } } > { __("add Teaser") } </Button>
      </PanelBody>

      <PanelBody title = { __("Teaser settings") } initialOpen = { true } >

      <SelectControl label = { __("Post type") } value = { this.props.attributes.post_type } onChange = { this.onChangePostType } options = { this.state.post_types } disabled = { this.state.disable_select } />
      <CheckboxControl label = { __("enable Webp") } checked = { this.props.attributes.enable_webp } onChange = { (isChecked) => { this.props.setAttributes( { enable_webp: isChecked } ) } } />
      <CheckboxControl label = { __("show image") } checked = { this.props.attributes.show_image } onChange = { (isChecked) => { this.props.setAttributes( { show_image: isChecked } ) } } />
      <CheckboxControl label = { __("show description") } checked = { this.props.attributes.show_excerpt } onChange = { (isChecked) => { this.props.setAttributes( { show_excerpt: isChecked } ) } } />
      <CheckboxControl label = { __("show button") } checked = { this.props.attributes.show_button } onChange = { (isChecked) => { this.props.setAttributes( { show_button: isChecked } ) } } />

      <TextControl label = { __("button text") } value = { this.props.attributes.default_button_text } onChange = { (value) => { this.props.setAttributes( { default_button_text: value } ) } } />

      </PanelBody>

      {
        this.state.teaser.map(teaser => {

          return (
            <PanelBody title = { teaser.title ?  teaser.title : __("New Teaser") } initialOpen = { false } key = { teaser.id } >
            <SelectControl label = { __("Teaser") } value = { teaser.selected_post } options = { options } onChange = { (e) => this.onChangeTeaser(e, teaser.id) } />

            <TextControl label = { __("Title") } value = { teaser.title } onChange = { (value) => this.onChangeTitle(value, teaser.id) } />
            <TextareaControl label = { __("Description") } value = { teaser.excerpt } onChange = { (value) => this.onChangeExcerpt(value, teaser.id) } />
            <TextControl label = { __("Button Text") } value = { teaser.button_text } onChange = { (value) => this.onChangeButton(value, teaser.id) }  />
            <Button isDefault onClick = { (e) => this.deleteTeaser(e, teaser.id) } > { __("remove") } </Button>
            </PanelBody>
          );
        })
      }


      </InspectorControls>
      <div className = { this.props.className } >
      {
        this.state.teaser.map(teaser => {

          var image, excerpt, button = "";

          if(this.props.attributes.show_image)
          {
            image = <img src = { teaser.image.src } alt = "thumbnail"/>;
          }

          if(this.props.attributes.show_excerpt)
          {
            excerpt = <p> { teaser.excerpt } </p>;
          }

          if(this.props.attributes.show_button)
          {
            button = <span className="button"> { teaser.button_text } </span>;
          }

          return (
            <article key={teaser.id}>
              { image }
              <div className="body">
                <h3> { teaser.title } </h3>
                { excerpt }
                { button }
              </div>
            </article>
          )
        })
      }

      </div>
      </Fragment>
    )
  }
}



registerBlockType("pars/teaser-block", {
  title: "Teaser Container",
  icon: "universal-access-alt",
  category: "common",

  attributes: {
    teaser: {
      type: "string"
    },
    post_type: {
      type: "string",
      default: "posts"
    },
    show_image: {
      type: "bool",
      default: true
    },
    show_excerpt: {
      type: "bool",
      default: true
    },
    show_button: {
      type: "bool",
      default: true
    },
    enable_webp: {
      type: "bool",
      default: false
    },
    default_button_text: {
      type: "string",
      default: __("more")
    }
  },

  edit: teaserBlockComponent,

  save()  {
    return null;
  }
});

import "./editor.scss";

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType} = wp.blocks;
const { InnerBlocks } = wp.editor;

registerBlockType("pars/container-block", {
  title: 'Container',
  category: "common",

  edit: (props) => {
    const { className } = props;
    return (
      <div className = { className } >
        <InnerBlocks />
      </div>
    );
  },

  save: (props) => {
    const { className } = props;
    return (
      <div className = { className } >
        <InnerBlocks.Content />
      </div>
    );
  },
});

<?php
/**
 * Plugin Name: Gutenberg Components
 * Description: — thanks to create-guten-block.
 * Author: Alex,
 * Author URI: https://parsmedia.info/
 * Version: 1.0.0
 *
 */

// Exit if accessed directly.
if(!defined('ABSPATH')) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';

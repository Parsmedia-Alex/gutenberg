<?php
/**
* Blocks Initializer
*
* Enqueue CSS/JS of all the blocks.
*
*/

//https://wordpress.org/gutenberg/handbook/designers-developers/developers

// Exit if accessed directly.
if(!defined( 'ABSPATH')) {
	exit;
}

//needed?

function enqueue_block_assets()
{
	// Styles.
	wp_enqueue_style('components-style-css', plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), array( 'wp-editor' )
	// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
);
}

// Hook: Frontend assets.
//add_action( 'enqueue_block_assets', 'enqueue_block_assets' );

function enqueue_editor_assets()
{
	wp_localize_script( 'wp-api', 'wpApiSettings', array(
		'root' => esc_url_raw(rest_url()),
		'nonce' => wp_create_nonce('wp_rest')
	) );

	wp_enqueue_script('teaser-cgb-block-js', plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ),
	array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-api' ), // Dependencies
	// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: File modification time.
	true
);

wp_enqueue_style('components-block-editor-css', plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), array( 'wp-edit-blocks' )
// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
);
}

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'enqueue_editor_assets' );


function render_teaser_block($attributes)
{
	$output = "";


	$teasers = json_decode($attributes["teaser"], true);

	//webp
	//picture
	//headline

	/*
	<picture>
		<source srcset>
		<img src alt>
	</picture>

	<picture>
		<source srcset media(sizes)>
		<img src alt>
	</picture>

	<picture>
		<source srcset media(sizes)>
		<img src srcset alt>
	</picture>

	<picture>
		<source srcset>
		<img src srcset sizes alt>
	</picture>

	*/


	$output .= "<section class='teaser_block'>";
	$output .= "<div class='grid_wrapper'>";

	foreach($teasers as $teaser)
	{

		$srcset = wp_get_attachment_image_srcset($teaser["image"]["id"], "teaser-size");
		//src, alt, srcset, sizes, width, height
		//$image = wp_get_attachment_image($teaser["image"]["id"], "teaser-size", false, array( "class" => "" ));

	$output .= "<article>";
	$output .= "<picture class='lazyload'>";
	//webp
	$output .= "";
		$output .= "<source data-srcset='" . $srcset . "'>";
		$output .= "<img data-src='" . $teaser["image"]["src"] ."' alt=''>";
	$output .= "</picture>";
	$output .= "<div class='body'>";
		$output .= "<a href='" . $teaser["url"] . "' title='" . $teaser["title"] . "'>";
			$output .= "<h3>" . $teaser["title"] . "</h3>";
		$output .= "</a>";
		$output .= "<p>" . $teaser["excerpt"] . "</p>";
		$output .= "<span class='button'>" . $teaser["button_text"] . "</span>";
	$output .= "</div>";
	$output .= "</article>";
}

$output .= "</div>";
$output .= "</section>";

return $output;
}


register_block_type("pars/teaser-block", array(

	"attributes" => array(
		"teaser" => array(
			"type" => "string"
		),
		"enable_webp" => array(
			"type" => "bool",
			"default" => false
		),
		"show_image" => array(
			"type" => "bool",
			"default" => true
		),
		"show_excerpt" => array(
			"type" => "bool",
			"default" => true
		),
		"show_button" => array(
			"type" => "bool",
			"default" => true
		),

	),
	"render_callback" => "render_teaser_block",
));

function block_theme_setup()
{
	add_image_size("teaser-size", 400, 400, array("center, center"));
}

add_action( 'after_setup_theme', 'block_theme_setup' );
